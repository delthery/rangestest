#-------------------------------------------------
#
# Project created by QtCreator 2016-07-07T15:35:30
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

TARGET = tst_rangestest
CONFIG   += console
CONFIG   -= app_bundle
CONFIG += c++11

TEMPLATE = app


SOURCES += tst_rangestest.cpp \
    stimuluspoint.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"

HEADERS += \
    stimuluspoint.h
