#ifndef STIMULUSPOINT_H
#define STIMULUSPOINT_H

#include <QObject>
#include <QMetaClassInfo>
#include <QPair>
#include <QList>
#include <QVector>
#include <QHash>
#include <QDebug>

class HardwarePoint;
class BasePoint;
class StimulusPoint;
class PortPoint;
class SParamPoint;

class FOM
{
public:
    bool Enabled;
    double Mul;
    double Div;
    double Shift;
    double Eval(double freq){
        if(Enabled) return freq * Mul / Div + Shift;
        return freq;
    }
    double EvalInv(double freq) {
        if(Enabled) return (freq - Shift) * Div / Mul;
    }
};


class HardwarePoint
{
public:
    struct PortReceiver
    {
        double Test;
        double Reference;        
    };

    int StimulusPort;
    int Index;
    QVector<PortReceiver> PortReceivers;        
};


class BaseRange : public QObject
{
    //Q_GADGET
    Q_OBJECT
public:
    BaseRange(int nPoints)
        : QObject(),
          NPoints(nPoints)//, Points(nPoints)
    {
    }
    virtual void Generate() = 0;
    int NPoints;
    QVector<BasePoint> Points;
};

Q_DECLARE_INTERFACE(BaseRange, "BaseRange")

/////////////////////////////////////////////////////////////

class StimulusRange : public BaseRange
{
    Q_GADGET
    //Q_OBJECT
public:
    StimulusRange() : BaseRange(0) {}

    void SetStart(double start) { _start = start; } //calc center width
    void SetStop(double stop) { _stop = stop; }
    void SetCenter(double c){ _center = c;}
    void SetWidth(double w){ _width = w;}
    void SetValue(double v){_value = v;}
    virtual void Generate() = 0;
    FOM Fom;

protected:
    double _start;
    double _stop;
    double _center;
    double _width;
    double _value;
};

class StimulusRangeProxy : public FOM
{
public:
    StimulusRangeProxy(StimulusRange *S) : originalStimulusRange(S){}
    StimulusRange* originalStimulusRange;
};

class FreqStimulusRange : public StimulusRange
{
    Q_GADGET
    Q_CLASSINFO("State", tr("Freq"))
public:
    Q_INVOKABLE FreqStimulusRange() :
        StimulusRange() {}
    void Generate(){
        qDebug() << "FreqStimulusRange::Create()";
    }
};
class PowerStimulusRange : public StimulusRange
{    
    Q_GADGET
    //Q_OBJECT
    Q_CLASSINFO("State", "Power")
public:
    Q_INVOKABLE PowerStimulusRange() :
        StimulusRange() {}
    void Generate(){
        qDebug() << "PowerStimulusRange::Create()";
    }
};
class SegmentStimulusRange : public StimulusRange
{
    Q_GADGET
    //Q_OBJECT
    Q_CLASSINFO("State", "Segment")
public:
    Q_INVOKABLE SegmentStimulusRange() :
        StimulusRange() {}
    void Generate(){
        qDebug() << "SegmentStimulusRange::Create()";
    }
};
class TestStimulusRange : public StimulusRange
{
    Q_GADGET
    //Q_OBJECT
    Q_CLASSINFO("Fake", "FakeState")
};

////////////////////////////////////////////////

class ReceiversRange
{
public:
    ReceiversRange(StimulusRange* stimulus, int stimulusPort, int receiversPort) :
        Stimulus(new StimulusRangeProxy(stimulus)),
        StimulusPort(stimulusPort),
        ReceiversPort(receiversPort)
    {
        //подписаться на изменения стимула
    }
    void SetPoint(int index, HardwarePoint::PortReceiver& point)
    {
        //if(point.StimulusPort != StimulusPort) return;

        T[index] = point.Test;
        R[index] = point.Reference;
    }
    bool FOMode(){ return _foMode; }
    void SetFOMode(bool mode) { _foMode = mode; }

    StimulusRangeProxy* Stimulus;
    int StimulusPort;
    int ReceiversPort;
    QVector<double> T;
    QVector<double> R;
private:
    bool _foMode;
};

class StimulusPortRange
{
public:
    StimulusPortRange(int stimulusPort, int nPorts) : StimulusPort(stimulusPort)
    {
        for(int i = 0; i < nPorts ;i++)
            Receivers.append(new ReceiversRange(Stimulus, stimulusPort, i));
    }

    void SetPoint(HardwarePoint& point)
    {
        if(point.StimulusPort != StimulusPort) return;
        for(int i = 0; i < Receivers.size(); ++i)
            Receivers[i]->SetPoint(point.Index, point.PortReceivers[i]);
    }

    bool FOMode(){ return _foMode;}
    void SetFOMode(bool fom) {
        _foMode = fom;
        for(int i = 0; i < Receivers.size(); ++i) {
            Receivers[i]->SetFOMode(fom);
        }
    }
    bool Active;
    int StimulusPort;
    StimulusRange* Stimulus;
    QVector<ReceiversRange*> Receivers;
private:
    bool _foMode;
};


class Range
{
public:
    Range(StimulusRange* S, int nPorts) : Stimulus(S)
    {
        for(int i = 0; i < nPorts; i++) {
            StimulusPort.append(new StimulusPortRange(i, nPorts));
        }
    }

    QVector<StimulusPortRange*> StimulusPort;

    StimulusRange* Stimulus;

    QList<double> S(int rcv, int src)
    {
        auto B = StimulusPort[src]->Receivers[rcv]->R;
        auto A = StimulusPort[src]->Receivers[src]->T;

        //TODO vector math: return B / A;
        return QList<double>();
    }
};

/////////////////////////////////////////////////////////////

// TODO imp mutator
class StateMutator
{ //TODO
public:

    StateMutator(QString key) : _key(key) {}

    void AddState(QMetaObject object){
        for(int i = 0; i < object.classInfoCount(); i++)
        {
            qDebug() << "StateMutator::AddState" <<
                        object.classInfo(i).name() <<
                        object.classInfo(i).value();
            if(object.classInfo(i).name() == _key) {                
                //_metaObjects.append(object); //check alread existed
                QObject* instance = object.newInstance();
                _hash.insert(
                            QString(object.classInfo(i).value()),
                            qMakePair(object, instance));
                return;
            }
        }
    }

    QObject* SetState(QString state) {
        if(_hash.contains(state)) {
            qDebug() << "Hash.Second" << _hash[state].second;
            _activeState = _hash[state].second;
        }

        return _activeState;
    }

private:
    //QVector<QMetaObject> _metaObjects;
    QHash<QString, QPair<QMetaObject, QObject*>> _hash;
    QString _key;
    QObject* _activeState;
};

/////////////////////////////////////////////////////////////

class BasePoint
{
public:
    BasePoint(BaseRange* range) : _range(range){}
protected:
    BaseRange* _range;
};

class StimulusPoint : public BasePoint
{    
public:        

    StimulusPoint(StimulusRange* range, double freq, double pwr, double bw, double delay = 0);
    double Freq() { return Range()->Fom.Eval(_freq); }
    void SetFreq(double freq) { _freq = Range()->Fom.EvalInv(freq); }
    double Power() { return _pwr; }
    void SetPower(double power) { _pwr = power; }

private:
    StimulusRange* Range() { static_cast<StimulusRange*>(_range); }
    double  _freq;
    double  _pwr;
    double  _bw;
    double  _delay;
};

class PortPoint
{
public:
    PortPoint(StimulusPoint* s, int p) : Stimulus(s), Port(p)
    {}
    int Port;
    StimulusPoint* Stimulus;
    double T; //test receiver
    double R; //reference receiver
};

class SParamPoint
{
public:
    SParamPoint(StimulusPoint* stimulus, int sPort, int nPorts) :
        StimulusPort(sPort),
        Stimulus(stimulus)
    {
        Q_UNUSED(nPorts)
        //Port.reserve(nPorts);
    }
    int                 StimulusPort;
    StimulusPoint*      Stimulus;
    //QVector<PortPoint>    Port;
};

class ReflectionPoint : public SParamPoint // src and rcv ports is equal
{
};

class TransmissionPoint : public SParamPoint
{
};

/////////////////////////////////////////////////////////////

template<typename T>
class Processor
{
public:
    Processor(bool disabled = false) :
        _disabled(disabled),
        _status(0)
    { }
    int Process(T* point) {
        if(_disabled) return 0;
        _status = Processing(point);
        return _status;
    }
    void Enable(){ _disabled = false; }
    void Disable(){ _disabled = true; }
    int Satus(){ return _status; }
protected:
    virtual int Processing(T* point) = 0;
private:
    bool _disabled;
    int _status;
};

class HardwarePointProcessor : public Processor<HardwarePoint>
{
public:
    HardwarePointProcessor(bool disabled) :
        Processor<HardwarePoint>(disabled) {}

    virtual int Processing(HardwarePoint &point)
    {
        Q_UNUSED(point)
        qDebug() << QStringLiteral("HWPointProcessor::Processing()");
        return 0;
    }
};

class HWPointProcessor1 : public HardwarePointProcessor
{
public:
    HWPointProcessor1(bool disabled) :
        HardwarePointProcessor(disabled) {}

    int Processing(HardwarePoint *point) /*override*/
    {
        Q_UNUSED(point)
        qDebug() << QStringLiteral("HWPointProcessor1::Processing()");
        return 0;
    }
};
class HWPointProcessor2 : public HardwarePointProcessor
{
public:
    HWPointProcessor2(bool disabled) :
        HardwarePointProcessor(disabled) {}

    int Processing(HardwarePoint *point) /*override*/
    {
        Q_UNUSED(point)
        qDebug() << QStringLiteral("HWPointProcessor2::Processing()");
        return 0;
    }
};

template<typename T>
class ProcessedData
{
public:
    ProcessedData() = default;

    void Add(Processor<T> *processor)
    {
        _processors.push_back(processor);
    }

    void Process(T* data)
    {
//        for(auto processor = _processors.begin(); processor < _processors.end(); ++processor) {
//            processor->Process(data);
//        }

        QVectorIterator<Processor<T>*> i(_processors);
        while(i.hasNext()){
            i.next()->Process(data);
        }
    }
    void DisableProcessors()
    {
//        for(auto processor = _processors.begin(); processor < _processors.end(); ++processor) {
//            processor->Disable();
//        }
        QVectorIterator<Processor<T>*> i(_processors);
        while(i.hasNext()){
            i.next()->Disable();
        }
    }
    void EnableProcessors()
    {
//        for(auto processor = _processors.begin(); processor < _processors.end(); ++processor) {
//            processor->Enable();
//        }
        QVectorIterator<Processor<T>*> i(_processors);
        while(i.hasNext()){
            i.next()->Enable();
        }
    }


private:
    QVector<Processor<T>*> _processors;
};

class ProcessedHardwarePoint : public ProcessedData<HardwarePoint>, public HardwarePoint
{
public:
    ProcessedHardwarePoint() : ProcessedData<HardwarePoint>(), HardwarePoint(){}

    void Process()
    {
        ProcessedData<HardwarePoint>::Process((HardwarePoint*) this);
    }
};

class HWPoint : public ProcessedHardwarePoint
{
public:
    HWPoint() : ProcessedHardwarePoint() {
        Add(new HWPointProcessor1(false));
        Add(new HWPointProcessor2(false));
    }
};

/////////////////////////////////////////////////////////////

template <class TIn>
class Input
{
public:
    Input() = default;
    virtual void Set(TIn* data) = 0;
};

template <class TOut>
class Output
{
public:
    Output() : _next (Q_NULLPTR) {}

    void SetNext(Input<TOut>* next) {
        _next = next;
    }
    bool HasNext(){ return _next != Q_NULLPTR;}
    Input<TOut>* Next(){ return _next; }
private:
    Input<TOut>* _next;
};

template <typename TIn, typename TOut = TIn>
class Stage : public Input<TIn>, public Output<TOut>
{
public:
    Stage(){}

    void Set( TIn* data )
    {
        _data = Process(data);
        if(HasNext()) Next()->Set(_data);
    }
    const TOut* Data(){ return _data; }
protected:
    virtual TOut* Process(TIn* data) = 0;

private:
    TOut* _data;
};

// child test classes

class Stage1 : public Stage<HardwarePoint, HardwarePoint>
{
public:
    Stage1(){}
    HardwarePoint* Process(HardwarePoint* data) {
        Q_UNUSED(data)
        qDebug() << "Stage1::Process()";
        return Q_NULLPTR;
    }
};

class Stage2: public Stage<HardwarePoint, SParamPoint>
{
public:
    Stage2(){}
    SParamPoint* Process(HardwarePoint *data) {
        Q_UNUSED(data)
        qDebug() << "Stage2::Process()";
        return Q_NULLPTR;
    }
};

class QStage2 : public QObject, public Stage2
{
    Q_OBJECT
public:
    QStage2() : QObject() {}
    SParamPoint* Process(HardwarePoint *data);

signals:
    void Changed();
};

class Stage3: public Stage<SParamPoint, SParamPoint>
{
public:
    Stage3() = default;
    SParamPoint* Process(SParamPoint *data)    {
        Q_UNUSED(data)
        qDebug() << "Stage3::Process()";
        return Q_NULLPTR;
    }
};

/////////////////////////////////////////////////////////////

class StageSignals : public QObject
{
    Q_OBJECT
public:
    StageSignals():QObject(){}
signals:
    void Changed();
    //TODO check variant with void ChangedData(void* data);
public slots:
    void OnChange()
    {
        qDebug() << "StageSignals::OnChange()";
    }
};

///
///
///
template<class TIn, class TOut = TIn>
class StageWithSignals :
        public StageSignals,
        public Stage<TIn, TOut>
{
public:
    StageWithSignals() = default;
    void Set( TIn* data )
    {
        Stage::Set(data);
        emit Changed();
    }
//signals:
//    void Changed();
};

///
/// \brief The StageWithSignals1 is test implementation of
///        StageWithSignals<> template with input and output
///        ports data type equal HardwarePoint
///
class StageWithSignals1 : public StageWithSignals<HardwarePoint>
{
    Q_OBJECT
public:
    StageWithSignals1(){}
    ///
    /// \brief Process Data processing implementation
    /// \param data processing input data with HardwarePoint type
    /// \return processed data with HardwarePoint type
    ///
    HardwarePoint* Process(HardwarePoint *data)
    {
        Q_UNUSED(data)
        qDebug() << "StageWithSignals1::Process()";

        emit ChangedPoint(data);

        return Q_NULLPTR;
    }
signals:
    void ChangedPoint(HardwarePoint* point);
};

#endif // STIMULUSPOINT_H
