#include <QString>
#include <QtTest>
#include "stimuluspoint.h"

class RangesTest : public QObject
{
    Q_OBJECT

public:
    RangesTest();

private Q_SLOTS:
    void testCase1();
    void testStateMutator();
    void testProcessor();
    void testStage();
};

RangesTest::RangesTest()
{
}

void RangesTest::testCase1()
{
    QVERIFY2(true, "Failure");
}

void RangesTest::testStateMutator()
{
    StateMutator state("State");

    state.AddState(TestStimulusRange::staticMetaObject);
    state.AddState(FreqStimulusRange::staticMetaObject);
    state.AddState(PowerStimulusRange::staticMetaObject);
    state.AddState(SegmentStimulusRange::staticMetaObject);

    QObject* activeState = state.SetState("Freq");
    if(activeState == Q_NULLPTR) {
        qDebug() << "ActiveState is null!";
        return;
    }
    qDebug() << "ActiveState"
             << activeState->metaObject()->classInfo(0).name()
             << activeState->metaObject()->classInfo(0).value();
    BaseRange* range;
    if(range = qobject_cast<BaseRange*>(activeState)) {
        range->Generate();
    }
    activeState = state.SetState("Power");
    qDebug() << "ActiveState" << activeState->metaObject()->classInfo(0).name()
             << activeState->metaObject()->classInfo(0).value();
    if(range = qobject_cast<BaseRange*>(activeState)) {
        range->Generate();
    }
}

void RangesTest::testProcessor()
{
    ProcessedHardwarePoint point;

    point.Add(new HWPointProcessor1(false));
    point.Add(new HWPointProcessor2(false));
    point.Add(new HWPointProcessor2(false));

    point.Process();
    qDebug() << "Disable processors";
    point.DisableProcessors();
    point.Process();
    qDebug() << "Enable processors";
    point.EnableProcessors();
    point.Process();

    qDebug() << "HWPoint with default processors";
    HWPoint hwPoint;
    hwPoint.Process();
}

void RangesTest::testStage()
{
    HardwarePoint point;
    point.Index = 27;

    Stage1* stage1 = new Stage1();
    //Stage2* stage2 = new Stage2();
    QStage2* stage2 = new QStage2();
    connect(stage2, &QStage2::Changed, [=](){qDebug() << "QStage2::Changed()";});
    Stage3* stage3 = new Stage3();

    stage1->SetNext(stage2);
    stage2->SetNext(stage3);
    //stage3->SetNext((Stage<SParamPoint, HardwarePoint>*)stage1);

    stage1->Set(&point);

    StageWithSignals1* stageWithSignals = new StageWithSignals1();
    connect(stageWithSignals, &StageWithSignals1::Changed, [=](){qDebug()<<"StageWithSignals1::Changed()";});
    connect(stageWithSignals, &StageWithSignals1::Changed, stageWithSignals, &StageWithSignals1::OnChange);

    connect(stageWithSignals, &StageWithSignals1::ChangedPoint, [=](HardwarePoint* data){qDebug()<<"StageWithSignals1::ChangedPoint()" << data->Index;});
    stageWithSignals->Set(&point);
}

QTEST_APPLESS_MAIN(RangesTest)

#include "tst_rangestest.moc"
